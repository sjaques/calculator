use core::panic;
use std::io;

fn read_equation_from_input() -> Vec<String> {
    let mut equation = String::new();

    io::stdin().read_line(&mut equation).expect("Missing input line");
    equation.split_whitespace().map(|w| String::from(w)).collect()
}

fn main() {
    println!("This is a simple calculator");
    let mut operation: Option<Box<dyn calculator::Operation>> = None;

    let equation = read_equation_from_input();

    let mut result = 0.0;

    for operand in equation {
        if let Ok(operand) = operand.parse::<f64>() {
            result = match &operation {
                Some(operation) => operation.calculate(result, operand),
                None => operand,
            };
            operation = None;
            print!("{} ", operand);
        } else if operand.len() == 1 {
            let op: &char = &operand.chars().next().unwrap();

            operation = calculator::determine_operation(op);
            if operation.is_none() {
                panic!("Missing operation");
            }
            print!("{} ", op);
        }
    }
    print!("= {}\n", result);
}
