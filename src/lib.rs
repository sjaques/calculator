use factorial::Factorial as Fact;
use num;

pub trait Operation {
    fn calculate(&self, x: f64, y: f64) -> f64;
    fn associative(&self) -> bool;
}

pub struct Addition {}
pub struct Substraction {}
struct Multiplication {}
struct Division {}
struct Exponentiation {}
struct Factorial {}

impl Operation for Addition {
    fn calculate(&self, x: f64, y: f64) -> f64 {
        x + y
    }
    fn associative(&self) -> bool {
        false
    }
}

impl Operation for Substraction {
    fn calculate(&self, x: f64, y: f64) -> f64 {
        x - y
    }
    fn associative(&self) -> bool {
        false
    }
}

impl Operation for Multiplication {
    fn calculate(&self, x: f64, y: f64) -> f64 {
        x * y
    }
    fn associative(&self) -> bool {
        true
    }
}

impl Operation for Division {
    fn calculate(&self, x: f64, y: f64) -> f64 {
        x / y
    }
    fn associative(&self) -> bool {
        true
    }
}

impl Operation for Exponentiation {
    fn calculate(&self, x: f64, y: f64) -> f64 {
        num::pow(x, y as usize)
    }
    fn associative(&self) -> bool {
        false
    }
}

impl Operation for Factorial {
    fn calculate(&self, _x: f64, y: f64) -> f64 {
        let number = if y == y.floor() {
            y as u64
        } else {
            panic!("Number is not an integer! {}", y)
        };
        match number.checked_factorial() {
            Some(fact) => fact as f64,
            None => panic!("Number too big! {}", y),
        }
    }
    fn associative(&self) -> bool {
        false
    }
}

pub fn determine_operation(operation: &char) -> Option<Box<dyn Operation>> {
    match operation {
        '+' => Some(Box::new(Addition {})),
        '-' => Some(Box::new(Substraction {})),
        '*' => Some(Box::new(Multiplication {})),
        '/' => Some(Box::new(Division {})),
        '^' => Some(Box::new(Exponentiation {})),
        '!' => Some(Box::new(Factorial {})),
        _ => None,
    }
}
